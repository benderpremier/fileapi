package me.damienfavre.algebras

import java.io._

import cats.effect._
import cats.implicits._
import me.damienfavre.model.FileApi.{LinuxFileSystemError, LinuxPath}

/**
  * Define the algebra to copy a file on a linux file system
  * @tparam F: the effect type in which the file copy is happening
  */
trait LinuxFileSystemAlgebra[F[_]] {
  def copy(
    from: LinuxPath,
    to: LinuxPath
  )(
    implicit concurrent: Concurrent[F]
  ): F[Either[LinuxFileSystemError, Unit]]
}

object LinuxFileSystemAlgebra {

  object IOLinuxFileSystem extends LinuxFileSystemAlgebra[IO] {

    import me.damienfavre.helpers.CatsEffectCopyFile

    def copy(
      from: LinuxPath,
      to: LinuxPath
    )(
      implicit concurrent: Concurrent[IO]
    ): IO[Either[LinuxFileSystemError, Unit]] = {
      for {
        _ <- IO.unit
        orig = new File(from.path)
        dest = new File(to.path)
        _ <- CatsEffectCopyFile.copy[IO](orig, dest)
      } yield ().asRight
    }
  }

}
