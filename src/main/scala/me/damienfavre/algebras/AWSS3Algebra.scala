package me.damienfavre.algebras

import cats.effect.IO
import cats.implicits._
import me.damienfavre.model.FileApi.{AWSS3FileSystemError, S3File}

import scala.concurrent.Future

trait AWSS3Algebra[F[_]] {
  def copy(from: S3File, to: S3File): F[Either[AWSS3FileSystemError, Unit]]
}

object AWSS3Algebra {
  object DummyAWSS3 extends AWSS3Algebra[IO] {

    def performCopy(
      from: S3File,
      to: S3File
    ): IO[Future[Unit]] = IO {
      println("Calling AWS API ...")
      //Assume we are calling aws apis here
      Future.successful(())
    }

    override def copy(
      from: S3File,
      to: S3File
    ): IO[Either[AWSS3FileSystemError, Unit]] = for {
      _ <- IO.fromFuture(performCopy(from,to))
    } yield ().asRight
  }
}
