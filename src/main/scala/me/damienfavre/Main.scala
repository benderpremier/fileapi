package me.damienfavre

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import fs2.Stream
import me.damienfavre.model.FileApi.{Command, Copy, LinuxPath, S3File}

object Main extends IOApp {

  // 1) let's assume we are getting a stream of commands from a source e.g. kafka
  val commands: Stream[IO, Command] = Stream(
    Copy(LinuxPath("test.txt"), LinuxPath("copied.txt")),
    Copy(S3File("bucket1", "test.txt"), S3File("bucket2", "test.txt")),
    Copy(S3File("bucket1", "test.txt"), LinuxPath("copied.txt")) // that one is not supported
  )

  // we'll want a proper logging framework
  def logError(e: Throwable): Stream[IO, Unit] = Stream.eval(
    IO(println(s"error: ${e.getMessage}"))
  )

  // 2) For every command we receive we want to evaluate it with our interpreter
  val program: Stream[IO, Unit] = commands
    .evalMap(CommandInterpreter.interpret)
    .handleErrorWith(logError)

  // 3) Finally we compile our Stream
  override def run(args: List[String]): IO[ExitCode] =
    program.compile.drain.as(ExitCode.Success)

}
