package me.damienfavre

import cats.effect.{Concurrent, IO}
import me.damienfavre.model.FileApi._
import me.damienfavre.algebras.LinuxFileSystemAlgebra.IOLinuxFileSystem
import me.damienfavre.algebras.AWSS3Algebra.DummyAWSS3

object CommandInterpreter {

  def interpret(command: Command)(implicit concurrent: Concurrent[IO]): IO[Unit] = {
    val res = command match {
      case Copy(s: LinuxPath, t: LinuxPath) => IOLinuxFileSystem.copy(s,t)
      case Copy(s: S3File, t: S3File) => DummyAWSS3.copy(s,t)
      case _ => IO {Left(OperationNotSupported)}
    }
    res.flatMap {
      case Right(_) => IO.unit
      case Left(e) => IO.raiseError(new Exception(s"Command failed with error: $e"))
    }
  }

}
