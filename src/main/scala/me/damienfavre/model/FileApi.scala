package me.damienfavre.model

object FileApi {

  // we create a trait to list all available source types
  sealed trait FileSource
  sealed trait LinuxFileSystem extends FileSource
  sealed trait AWSS3FileSystem extends FileSource

  // we model the linux file as just a source
  sealed trait Path
  case class LinuxPath(path: String) extends Path
  case class S3File(bucket: String, fileName: String) extends Path

  //we model the different errors we can get
  sealed trait FileApiError
  case object OperationNotSupported extends FileApiError
  sealed trait LinuxFileSystemError extends FileApiError
  case object FileDoesNotExist extends LinuxFileSystemError
  sealed trait AWSS3FileSystemError extends FileApiError
  case class AccessDenied(bucket: String) extends AWSS3FileSystemError

  sealed trait Command
  case class Copy(source: Path, destination: Path) extends Command
  //Add more commands e.g. Delete, Rename

}
