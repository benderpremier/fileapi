## File Transfer API
This project demonstrates a file transfer API. We introduce two alebras:
1. One algebra to copy file on Linux File System
1. One algebra to copy file on AWS

The two algebras have a tagless final approach. 
#### A) Model

The model is rather simple and can be found in `model/FileApi`. It consist of 
- two types of FileSystem
- an ADT for expressing a file path in each FileSystem
- an ADT for the errors we can potentially encounter
- The list of commands accepted by our API

#### B) Alebras

I define an algebra, abstracting over the effect type. Then I implement it for a concrete effect. I have chose the cat-effect `IO` monad. I then have a `CommandInterpreter` object that glue together the different algebras. It pattern match on the commands and route them to te correct algebra.
 
#### C) Application
To simulate an environment in which we receive a stream of commands, the applications consist of a `FS2` stream of command that we interpret with the `CommandInterpreter`
The demo has 3 commands in it, one to copy the `test.txt` file, another to simulate calling the aws api and finally one that simulate a failing command.

To run the project simply `sbt run` and you should see:
- the `test.txt` file be copied to `copied.txt`
- `Calling AWS API ...` be displayed on the command line (simulating copying on aws)
- `error: Command failed with error: OperationNotSupported` simulating a failing command
